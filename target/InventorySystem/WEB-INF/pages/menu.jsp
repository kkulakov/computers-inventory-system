<div align = "center" class="buttons">
    <h3>
        <a href="${pageContext.request.contextPath}/index" class="button">Головна</a>
        <a href="${pageContext.request.contextPath}/browse" class="button">Перегляд</a>
        <a href="${pageContext.request.contextPath}/add/computer" class="button">Додати комп'ютер</a>
        <a href="${pageContext.request.contextPath}/add/equipment" class="button">Додати обладнання</a>
        <a href="${pageContext.request.contextPath}/add/classroom" class="button">Додати кабінет</a>
        <a href="${pageContext.request.contextPath}/logout" class="button">Вийти</a>
    </h3>
</div>