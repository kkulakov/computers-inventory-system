<%--@elvariable id="computer" type="ru.kulakov.inventory.domain.Computer"--%>
<%--@elvariable id="historyList" type="java.util.List"--%>
<%--@elvariable id="classroomList" type="java.util.List"--%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Комп'ютер: ${computer.inventoryNumber}</title>
        <link rel="stylesheet" href="/resources/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <script src="/resources/js/jquery-1.10.2.js"></script>
        <script src="/resources/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="/resources/js/custom/showComputer.js"></script>
    </head>
    <body>
    <%@include file="menu.jsp" %>
    <br>
    <br>
    <br>
    <br>
    <div align="center">
        <form:form method="PUT" id = "editComputerForm" commandName="computer"
                   action="${pageContext.request.contextPath}/show/computer/${computer.inventoryNumber}">
            <table>
                <tr>
                    <td>Інвентарний номер:
                    <td><form:input path="inventoryNumber" type="text" disabled = "true"
                                    value="${computer.inventoryNumber}"/>
                </tr>
                <tr>
                    <td>Процесор:
                    <td><form:input path="processor" type="text" autofocus="1" value="${computer.processor}"/>
                </tr>
                <tr>
                    <td>Материнська плата:
                    <td><form:input path="motherboard" type="text" value="${computer.motherboard}"/>
                </tr>
                <tr>
                    <td>Оперативна пам'ять:
                    <td><form:input path="ram" type="text" value="${computer.ram}"/>
                </tr>
                <tr>
                    <td>Жорсткий диск:
                    <td><form:input path="hdd" type="text" value="${computer.hdd}"/>
                </tr>
                <tr>
                    <td>Аудиторія:
                    <td>
                        <form:select path="classroom" class="classroom">
                            <form:options items="${classroomList}" itemLabel="number" itemValue="number" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td>Інше:
                    <td><form:input path="other" type="text" value="${computer.other}"/>
                </tr>
                <tr>
                    <td>
                    <td><input type="submit" value="Зберегти"/>
                </tr>
            </table>
        </form:form>
        <br>
        <br>
        <button onclick="duplicateEntry()">Дублювати запис</button>
        <input type="text" id="duplicateInventoryNumber" value="0">
        <div id="dialog-confirm" title="&nbsp&nbspДублювати запис?" style="display: none; background: LightSalmon;">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Дублювання запису призведе до запису до БД аналогічного &nbsp&nbsp&nbsp&nbsp&nbspкомп'ютера, але з іншим &nbsp&nbsp&nbsp&nbsp&nbspінвентарним номером. Це саме &nbsp&nbsp&nbsp&nbsp&nbspте, чого ви хочете?<br><br></p>
        </div>
        <div id = "success" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
            <p>
                Дані про комп'ютер ${computer.inventoryNumber} успішно оновлено.
            </p>
        </div>
        <div id = "successDuplicated" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
            <p>
               Комп'ютер ${computer.inventoryNumber} успішно дублікован.
            </p>
        </div>
        <div id = "fail" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Помилка! :(</h3>
            <p>
                Дані не були збережені.
            </p>
        </div>
        <c:if test="${!empty historyList}">
            <br>
            <br>
            <br>
            <h3>Історія змін комп'ютера:</h3>
            <table border="1">
                <tr>
                    <th>Процесор
                    <th>Материнська плата
                    <th>Оперативна пам'ять
                    <th>Жорсткий диск
                    <th>Аудиторія
                    <th>Інше
                    <th>Дата зміни
                    <th>
                </tr>
                <c:forEach items="${historyList}" var="history">
                    <tr>
                        <td>${history.processor}</td>
                        <td>${history.motherboard}</td>
                        <td>${history.ram}</td>
                        <td>${history.hdd}</td>
                        <td>${history.classroom.number}</td>
                        <td>${history.other}</td>
                        <td><fmt:formatDate value="${history.writingTime}" pattern="dd.MM.yy HH:mm" /></td>
                        <td>
                            <input type="button" value="Відновити" onclick="updateComputer('${history.processor}',
                                    '${history.motherboard}', '${history.ram}', '${history.hdd}',
                                    '${history.classroom.number}','${history.other}')">
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        </div>
    </body>
</html>