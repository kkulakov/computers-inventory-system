$(document).ready(function() {

    //making autocompletion
    var processor = [];
    var motherboard = [];
    var ram = [];
    var hdd = [];
    $.get( "getComputerList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            processor.push(data[i].processor);
            motherboard.push(data[i].motherboard);
            ram.push(data[i].ram);
            hdd.push(data[i].hdd);
        }
        $( "#processor" ).autocomplete({source: processor});
        $( "#motherboard" ).autocomplete({source: motherboard});
        $( "#ram" ).autocomplete({source: ram});
        $( "#hdd" ).autocomplete({source: hdd});
    });
    var model = [];
    var description = [];
    $.get( "getEquipmentList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            model.push(data[i].model);
            description.push(data[i].description);
        }
        $( "#model" ).autocomplete({source: model});
        $( "#description" ).autocomplete({source: description});
    });






    $( "#success" ).hide();
    $( "#fail" ).hide();

    $('input[type=radio][name=typeSwitch]').change(function() {
        if (this.value == 'computer') {
            $('#equipmentForm').css('display', 'none');
            $('#computerForm').css('display', 'block');
        }
        else if (this.value == 'equipment') {
            $('#computerForm').css('display', 'none');
            $('#equipmentForm').css('display', 'block');
        }
    });

    var computerSelect = $('#computer');
    var allComputersOptions = computerSelect.children();
    $('.equipment_classroom').change(function() {
        var classroom = $('.equipment_classroom').val();

        allComputersOptions.detach().each(function(){
            var val = $(this).attr('id');
            if (val == '' || val == classroom) {
                $(this).appendTo(computerSelect);
            }
        });
        computerSelect.val('');
        if(computerSelect.has('option').length == 0){
            allComputersOptions.each(function() {
                $(this).appendTo(computerSelect);
            })
        }
        $("#computer").find(":first").attr("selected", "selected");
    });

    //sending forms via AJAX
    //computer form
    $('#computerForm').submit(function(event) {

        var inventoryNumber = $('.computerInventoryNumber').val();
        var processor = $('#processor').val();
        var motherboard = $('#motherboard').val();
        var ram = $('#ram').val();
        var hdd = $('#hdd').val();
        var classroom = $('#classroom').val();
        var other = $('#other').val();
        var json = {
            "inventoryNumber" : inventoryNumber,
            "processor" : processor,
            "motherboard" : motherboard,
            "ram": ram,
            "hdd": hdd,
            "classroom": {"number": classroom},
            "other": other
        };

        $.ajax({
            url: $("#computerForm").attr( "action"),
            data: JSON.stringify(json),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(computer) {
                if (computer != null && computer != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });
    //equipmentform
    $('#equipmentForm').submit(function(event) {

        var inventoryNumber = $('.equipmentInventoryNumber').val();
        var type = $('#type').val();
        var model = $('#model').val();
        var description = $('#description').val();
        var classroom = $('.equipment_classroom').val();
        var computer = $('#computer').val();
        var json = {
            "inventoryNumber": inventoryNumber,
            "type": {"code" : type},
            "model": model,
            "description": description,
            "classroom": {"number": classroom},
            "computer": {inventoryNumber: computer}};

        $.ajax({
            url: $("#equipmentForm").attr( "action"),
            data: JSON.stringify(json),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(equipment) {
                if (equipment != null && equipment != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });
});

function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }

}
