$(document).ready(function() {
    $( "#success" ).hide();
    $( "#successDuplicated" ).hide();
    $( "#fail" ).hide();

    var model = [];
    var description = [];
    $.get( "/getEquipmentList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            model.push(data[i].model);
            description.push(data[i].description);
        }
        $( "#model" ).autocomplete({source: model});
        $( "#description" ).autocomplete({source: description});
    });


    $('#editEquipmentForm').submit(function(event) {

        var type = $('#type').val();
        var model = $('#model').val();
        var description = $('#description').val();
        var classroom = $('#classroom').val();
        var computer = $('#computer').val();
        var json = {
            "type": {"code" : type},
            "model": model,
            "description": description,
            "classroom": {"number": classroom},
            "computer": {inventoryNumber: computer}
        };

        $.ajax({
            url: $("#editEquipmentForm").attr( "action"),
            data: JSON.stringify(json),
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(equipment) {
                if (equipment != null || equipment != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });
    //Showing computers in 1 cabinet
    var computerSelect = $('#computer');
    var allComputersOptions = computerSelect.children();

    $('#classroom').change(classroomChanged);
    function classroomChanged() {
        var classroom = $('#classroom').val();

        allComputersOptions.detach().each(function(){
            var val = $(this).attr('id');
            if (val == '' && val == classroom) {
                $(this).appendTo(computerSelect);
            }
        });
        computerSelect.val('');
        if(computerSelect.has('option').length == 0){
            allComputersOptions.each(function() {
                $(this).appendTo(computerSelect);
            })
        }
        $("#computer").find(":first").attr("selected", "selected");
    }

    classroomChanged();

    //setting last inventory number for duplicate function
    $.get( "/show/getlastid", function( data ) {
        $( '#duplicateInventoryNumber' ).val(data);
    });
});
function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }

}

function updateEquipment (type,  model, description, classroom, computer){
    var json = {
        "type": {"code" : type},
        "model": model,
        "description": description,
        "classroom": {"number": classroom},
        "computer": {inventoryNumber: computer}
    };

    $.ajax({
        url: $("#editEquipmentForm").attr( "action"),
        data: JSON.stringify(json),
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(equipment) {
            if (equipment != null && equipment != undefined)
                location.reload(); //this line is different
            else
                showBlock("#fail");
        },
        error: function () {
            showBlock("#fail");
        }
    });
}

function duplicateEntry() {
    $( "#dialog-confirm" ).dialog({
        dialogClass: "no-close",
        resizable: false,
        height:225,
        modal: true,
        buttons: {
            "Дублювати": function() {
                var inventoryNumber = $('#duplicateInventoryNumber').val();
                var type = $('#type').val();
                var model = $('#model').val();
                var description = $('#description').val();
                var classroom = $('#classroom').val();
                var computer = $('#computer').val();
                var json = {
                    "inventoryNumber": inventoryNumber,
                    "type": {"code" : type},
                    "model": model,
                    "description" : description,
                    "classroom": {"number": classroom},
                    "computer": {inventoryNumber: computer}
                };

                $.ajax({
                    url: "/add/equipment",
                    data: JSON.stringify(json),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(equipment) {
                        if (equipment != null && equipment != undefined){
                            showBlock("#successDuplicated");
                            $('#duplicateInventoryNumber').val(parseInt($('#duplicateInventoryNumber').val()) + 1);
                        }
                        else
                            showBlock("#fail");
                    },
                    error: function () {
                        showBlock("#fail");
                    }
                });
                $( this ).dialog( "close" );
            },
            "Відмінити": function() {
                $( this ).dialog( "close" );
            }
        }
    });
}