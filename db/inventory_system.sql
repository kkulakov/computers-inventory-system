﻿# Host: localhost  (Version: 5.6.17)
# Date: 2014-04-18 16:40:21
# Generator: MySQL-Front 5.3  (Build 4.115)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "classroom"
#

CREATE TABLE `classroom` (
  `number` int(11) NOT NULL DEFAULT '209',
  `xsize` int(11) NOT NULL DEFAULT '0',
  `ysize` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "classroom"
#

INSERT INTO `classroom` VALUES (209,10,10),(210,10,10),(211,19,2);

#
# Structure for table "computer"
#

CREATE TABLE `computer` (
  `inventory_number` int(11) NOT NULL DEFAULT '0',
  `processor` varchar(255) NOT NULL DEFAULT 'Intel',
  `motherboard` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `hdd` varchar(255) DEFAULT NULL,
  `classroom` int(11) NOT NULL DEFAULT '0',
  `xposition` int(11) NOT NULL DEFAULT '0',
  `yposition` int(11) NOT NULL DEFAULT '0',
  `other` varchar(255) DEFAULT NULL,
  `adding_time` datetime DEFAULT NULL,
  PRIMARY KEY (`inventory_number`),
  KEY `FKDC497F1B542005B4` (`classroom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "computer"
#

INSERT INTO `computer` VALUES (123,'Intel Core2Duo','Asus H87-PRO','Kingston DDR3-1600 8192MB ','Transcend StoreJet 25M3 1TB ',210,0,0,'2342','2014-05-05 20:08:54'),(321,'AMD Phenom x3','Asus H81M-E','Kingston DDR3-1600 4096MB','Transcend StoreJet 25A3 500GB',209,0,0,'dsff','2014-05-05 20:08:54'),(987,'AMD','Gigabyte GA-H87-HD3','G.Skill DDR3-2133 8192MB','Transcend StoreJet 25A3 500GB',210,0,0,'Wi-F Adapter','2014-05-06 20:08:54'),(999,'Intel i3','Asus Sabertooth','GeIL DDR3-1600 16384MB','Western Digital My Passport Ultra 1TB\r\n',209,0,0,'','1994-06-06 00:00:00'),(87655,'Intel i5','MSI Z77A-G43','GeIL DDR3-1600 16384MB','Toshiba 500GB 7200rpm',210,0,0,'','2014-04-06 20:08:54'),(87656,'AMD Athlon x64','Asus Vanguard B85','GeIL DDR3-1600 16384MB','Western Digital My Passport Ultra 1TB\r\n',210,0,0,'','2014-04-06 20:17:03'),(87657,'Intel Core2Duo','Gigabyte GA-H87-HD3','Kingston DDR3-1600 8192MB ','Transcend StoreJet 25M3 1TB ',210,0,0,'','2014-04-08 13:25:15');

#
# Structure for table "computer_history"
#

CREATE TABLE `computer_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_number` int(11) NOT NULL DEFAULT '0',
  `processor` varchar(255) NOT NULL DEFAULT 'Intel',
  `motherboard` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `hdd` varchar(255) DEFAULT NULL,
  `classroom` int(11) NOT NULL DEFAULT '0',
  `xposition` int(11) NOT NULL DEFAULT '0',
  `yposition` int(11) NOT NULL DEFAULT '0',
  `other` varchar(255) DEFAULT NULL,
  `writing_time` datetime NOT NULL DEFAULT '2014-04-07 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FKDD61BF70542005B4` (`classroom`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "computer_history"
#


#
# Structure for table "equipment_history"
#

CREATE TABLE `equipment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_number` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `model` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `bind_to` int(11) DEFAULT '-1',
  `classroom` int(11) NOT NULL DEFAULT '0',
  `xposition` int(11) NOT NULL DEFAULT '0',
  `yposition` int(11) NOT NULL DEFAULT '0',
  `writing_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK623802E3A709060A` (`bind_to`),
  KEY `FK623802E340284A6` (`type`),
  KEY `FK623802E3542005B4` (`classroom`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "equipment_history"
#


#
# Structure for table "equipment_types"
#

CREATE TABLE `equipment_types` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Data for table "equipment_types"
#

INSERT INTO `equipment_types` VALUES (1,'Монитор'),(2,'Комп\'ютерна миша'),(3,'Клавіатура'),(4,'Мережевий фільтр'),(5,'Принтер');

#
# Structure for table "equipment"
#

CREATE TABLE `equipment` (
  `inventory_number` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `model` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `bind_to` int(11) DEFAULT '-1',
  `classroom` int(11) NOT NULL DEFAULT '0',
  `xposition` int(11) NOT NULL DEFAULT '0',
  `yposition` int(11) NOT NULL DEFAULT '0',
  `adding_time` datetime DEFAULT NULL,
  PRIMARY KEY (`inventory_number`),
  KEY `FK4027E58EA709060A` (`bind_to`),
  KEY `type` (`type`),
  KEY `FK4027E58E542005B4` (`classroom`),
  KEY `FK4027E58E40284A6` (`type`),
  CONSTRAINT `FK4027E58E40284A6` FOREIGN KEY (`type`) REFERENCES `equipment_types` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "equipment"
#

INSERT INTO `equipment` VALUES (385,4,'X8','',123,210,0,0,'2014-03-30 02:12:08'),(543,4,'AP-52','',123,210,0,0,'2014-03-30 01:54:12'),(654,3,'WD-600','',123,210,0,0,'2014-03-30 15:17:45'),(656,4,'TRYT','',321,209,0,0,'2014-03-30 15:14:41'),(888,1,'SN-700','test',123,210,0,0,'2014-03-15 15:14:41'),(56643,3,'A4 Tech','standart keyboard',321,209,0,0,'2014-03-30 15:27:54'),(87654,3,'X7','standart',123,210,0,0,'2014-03-30 02:02:10');
