package ru.kulakov.inventory.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "equipment")
public class Equipment {
    @Id
    @Column(name = "inventory_number")
    private int inventoryNumber;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type")
    private Type type;
    @Column(name = "model")
    private String model;
    @Column(name = "description")
    private String description;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bind_to")
    private Computer computer;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "classroom")
    private Classroom classroom;
    @Column(name = "xposition")
    private int xPosition;
    @Column(name = "yposition")
    private int yPosition;
    @Column(name = "adding_time")
    private Date addingTime;

    public int getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(int inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Date getAddingTime() {
        return addingTime;
    }

    public void setAddingTime(Date addingTime) {
        this.addingTime = addingTime;
    }
}

