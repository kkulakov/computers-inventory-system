package ru.kulakov.inventory.domain;

import javax.persistence.*;

@Entity
@Table(name = "equipment_types")
public class Type {
    @Id
    @GeneratedValue
    @Column(name = "code")
    private int code;
    @Column(name = "type")
    private String typeName;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
