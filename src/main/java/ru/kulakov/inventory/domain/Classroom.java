package ru.kulakov.inventory.domain;

import javax.persistence.*;

@Entity
@Table(name = "classroom")
public class Classroom {
    @Id
    @Column(name = "number")
    private int number;
    @Column(name = "ysize")
    private int verticalSize;
    @Column(name = "xsize")
    private int horizontalSize;




    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getVerticalSize() {
        return verticalSize;
    }

    public void setVerticalSize(int verticalSize) {
        this.verticalSize = verticalSize;
    }

    public int getHorizontalSize() {
        return horizontalSize;
    }

    public void setHorizontalSize(int horizontalSize) {
        this.horizontalSize = horizontalSize;
    }
}
