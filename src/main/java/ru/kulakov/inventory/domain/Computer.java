package ru.kulakov.inventory.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "computer")
public class Computer {
    @Id
    @Column(name = "inventory_number")
    private int inventoryNumber;
    @Column(name = "processor")
    private String processor;
    @Column(name = "motherboard")
    private String motherboard;
    @Column(name = "ram")
    private String ram;
    @Column(name = "hdd")
    private String hdd;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "classroom")
    private Classroom classroom;
    @Column(name = "xposition")
    private int xPosition;
    @Column(name = "yposition")
    private int yPosition;
    @Column(name = "other")
    private String other;
    @Column(name = "adding_time")
    private Date addingTime;



    public int getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(int inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }
    public String getHdd() {
        return hdd;
    }

    public void setHdd(String hdd) {
        this.hdd = hdd;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Date getAddingTime() {
        return addingTime;
    }

    public void setAddingTime(Date addingTime) {
        this.addingTime = addingTime;
    }
}
