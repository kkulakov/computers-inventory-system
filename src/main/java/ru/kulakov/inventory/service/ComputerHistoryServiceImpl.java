package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.ComputerHistoryDAO;
import ru.kulakov.inventory.domain.Computer;
import ru.kulakov.inventory.domain.ComputerHistory;

import java.util.Date;
import java.util.List;

@Service
public class ComputerHistoryServiceImpl implements ComputerHistoryService {
    private static final int MAX_HISTORY_SIZE = 9;
    @Autowired
    ComputerHistoryDAO computerHistoryDAO;
    @Transactional
    public void addComputerToHistory(Computer computer) {
        ComputerHistory computerHistory = new ComputerHistory();
        computerHistory.setInventoryNumber(computer.getInventoryNumber());
        computerHistory.setProcessor(computer.getProcessor());
        computerHistory.setMotherboard(computer.getMotherboard());
        computerHistory.setHdd(computer.getHdd());
        computerHistory.setRam(computer.getRam());
        computerHistory.setClassroom(computer.getClassroom());
        computerHistory.setOther(computer.getOther());
        computerHistory.setWritingTime(new Date());

        int historySize = computerHistoryDAO.getHistorySizeForComputer(computer.getInventoryNumber());
        if(historySize > MAX_HISTORY_SIZE) {
            computerHistoryDAO.deleteComputerFromHistory(computerHistoryDAO.getFirstComputerFromHistory());
        }

        computerHistoryDAO.addComputerToHistory(computerHistory);
    }

    @Transactional
    public List<ComputerHistory> getHistoryList(int id) {
        return computerHistoryDAO.getHistoryList(id);
    }
}
