package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.TypeDAO;
import ru.kulakov.inventory.domain.Type;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    TypeDAO typeDAO;

    @Transactional
    public void addType(Type type) {
        typeDAO.addType(type);
    }

    @Transactional
    public Type getType(int code) {
        return typeDAO.getType(code);
    }

    @Transactional
    public List<Type> typeList() {
        return typeDAO.typeList();
    }
}
