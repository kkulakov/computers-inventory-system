package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.EquipmentDAO;
import ru.kulakov.inventory.domain.Equipment;

import java.util.Date;
import java.util.List;

@Service
public class EquipmentServiceImpl implements EquipmentService {
    @Autowired
    EquipmentDAO equipmentDAO;

    @Transactional
    public void addEquipment(Equipment equipment) {
        equipment.setAddingTime(new Date());
        equipmentDAO.addEquipment(equipment);
    }

    @Transactional
    public Equipment getEquipment(int inventoryNumber) {
        return equipmentDAO.getEquipment(inventoryNumber);
    }

    @Transactional
    public int getNextInventoryNumber() {
        return equipmentDAO.getNextInventoryNumber();
    }

    @Transactional
    public int getEquipmentCount() {
        return equipmentDAO.getEquipmentCount();
    }

    @Transactional
    public List<Equipment> equipmentList() {
        return equipmentDAO.equipmentList();
    }

    @Transactional
    public Equipment updateEquipment(Equipment equipment) {
        equipmentDAO.updateEquipment(equipment);
        return equipment;
    }

    @Transactional
    public void removeEquipment(int inventoryNumber) {
        equipmentDAO.removeEquipment(inventoryNumber);
    }
}
