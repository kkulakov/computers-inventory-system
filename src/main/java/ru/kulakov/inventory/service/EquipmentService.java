package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Equipment;

import java.util.List;

public interface EquipmentService {
    public void addEquipment(Equipment equipment);
    public Equipment getEquipment(int inventoryNumber);
    public int getNextInventoryNumber();
    public int getEquipmentCount();
    public List<Equipment> equipmentList();
    public Equipment updateEquipment(Equipment equipment);
    public void removeEquipment(int inventoryNumber);
}
