package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Computer;

import java.util.List;

public interface ComputerService {
    public void addComputer(Computer computer);
    public Computer getComputer(int inventoryNumber);
    public int getNextInventoryNumber();
    public int getComputerCount();
    public List<Computer> computerList();
    public Computer updateComputer(Computer computer);
    public void removeComputer(int inventoryNumber);
}
