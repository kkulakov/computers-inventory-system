package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Type;

import java.util.List;

public interface TypeService {
    public void addType(Type type);
    public Type getType(int code);
    public List<Type> typeList();
}
