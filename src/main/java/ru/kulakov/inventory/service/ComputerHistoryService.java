package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Computer;
import ru.kulakov.inventory.domain.ComputerHistory;

import java.util.List;

public interface ComputerHistoryService {
    public void addComputerToHistory(Computer computer);
    public List<ComputerHistory> getHistoryList(int id);
}
