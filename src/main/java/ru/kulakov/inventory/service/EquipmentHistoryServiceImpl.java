package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.EquipmentHistoryDAO;
import ru.kulakov.inventory.domain.Equipment;
import ru.kulakov.inventory.domain.EquipmentHistory;

import java.util.Date;
import java.util.List;

@Service
public class EquipmentHistoryServiceImpl implements EquipmentHistoryService {
    private static final int MAX_HISTORY_SIZE = 9;
    @Autowired
    EquipmentHistoryDAO equipmentHistoryDAO;

    @Transactional
    public void addEquipmentToHistory(Equipment equipment) {
        EquipmentHistory equipmentHistory = new EquipmentHistory();
        equipmentHistory.setInventoryNumber(equipment.getInventoryNumber());
        equipmentHistory.setType(equipment.getType());
        equipmentHistory.setModel(equipment.getModel());
        equipmentHistory.setDescription(equipment.getDescription());
        equipmentHistory.setComputer(equipment.getComputer());
        equipmentHistory.setClassroom(equipment.getClassroom());
        equipmentHistory.setWritingTime(new Date());

        int historySize = equipmentHistoryDAO.getHistorySizeForEquipment(equipment.getInventoryNumber());
        if(historySize > MAX_HISTORY_SIZE) {
            equipmentHistoryDAO.deleteEquipmentFromHistory(equipmentHistoryDAO.getFirstEquipmentFromHistory());
        }

        equipmentHistoryDAO.addEquipmentToHistory(equipmentHistory);
    }

    @Transactional
    public List<EquipmentHistory> getHistoryList(int  id) {
        return equipmentHistoryDAO.getHistoryList(id);
    }
}
