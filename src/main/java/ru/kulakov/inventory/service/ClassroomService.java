package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Classroom;

import java.util.List;

public interface ClassroomService {
    public void addClassroom(Classroom classroom);
    public Classroom getClassroom(int number);
    public List<Classroom> getClassroomList();
    public void deleteClassroom(int number);
}
