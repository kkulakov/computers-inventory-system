package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.ComputerDAO;
import ru.kulakov.inventory.domain.Computer;

import java.util.Date;
import java.util.List;

@Service
public class ComputerServiceImpl implements ComputerService{
    @Autowired
    ComputerDAO computerDAO;

    @Transactional
    public void addComputer(Computer computer) {
        computer.setAddingTime(new Date());
        computerDAO.addComputer(computer);
    }

    @Transactional
    public Computer getComputer(int inventoryNumber) {
        return computerDAO.getComputer(inventoryNumber);
    }

    @Transactional
    public int getNextInventoryNumber() {
        return computerDAO.getNextInventoryNumber();
    }

    @Transactional
    public int getComputerCount() {
        return computerDAO.getComputerCount();
    }

    @Transactional
    public List<Computer> computerList() {
        return computerDAO.computerList();
    }

    @Transactional
    public Computer updateComputer(Computer computer) {
//        if(getComputer(computer.getInventoryNumber()) == null)
//            return null;
        computerDAO.updateComputer(computer);
        return computer;
    }

    @Transactional
    public void removeComputer(int inventoryNumber) {
        computerDAO.removeComputer(inventoryNumber);
    }
}
