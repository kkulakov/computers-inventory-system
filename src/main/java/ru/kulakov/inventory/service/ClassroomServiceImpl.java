package ru.kulakov.inventory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kulakov.inventory.dao.ClassroomDAO;
import ru.kulakov.inventory.domain.Classroom;

import java.util.List;

@Service
public class ClassroomServiceImpl implements ClassroomService {
    @Autowired
    ClassroomDAO classroomDAO;

    @Transactional
    public void addClassroom(Classroom classroom) {
        classroomDAO.addClassroom(classroom);
    }

    @Transactional
    public Classroom getClassroom(int number) {
        return classroomDAO.getClassroom(number);
    }

    @Transactional
    public List<Classroom> getClassroomList() {
        return classroomDAO.getClassroomList();
    }

    @Transactional
    public void deleteClassroom(int number) {
        classroomDAO.deleteClassroom(number);
    }
}
