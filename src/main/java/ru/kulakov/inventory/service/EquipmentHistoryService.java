package ru.kulakov.inventory.service;

import ru.kulakov.inventory.domain.Equipment;
import ru.kulakov.inventory.domain.EquipmentHistory;

import java.util.List;

public interface EquipmentHistoryService {
    public void addEquipmentToHistory(Equipment equipment);
    public List<EquipmentHistory> getHistoryList(int id);
}
