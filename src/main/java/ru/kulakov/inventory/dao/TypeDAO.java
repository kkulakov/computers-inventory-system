package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.Type;

import java.util.List;

public interface TypeDAO {
    public void addType(Type type);
    public Type getType(int code);
    public List<Type> typeList();
}
