package ru.kulakov.inventory.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;

import ru.kulakov.inventory.domain.Computer;

import java.util.List;

@Repository
public class ComputerDAOImpl implements ComputerDAO{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addComputer(Computer computer) {
        sessionFactory.getCurrentSession().save(computer);
    }

    @Override
    public Computer getComputer(int inventoryNumber) {
        return (Computer) sessionFactory.getCurrentSession().get(Computer.class, inventoryNumber);
    }

    @Override
    public int getNextInventoryNumber() {
        Number maxNumber = (Number) sessionFactory.getCurrentSession()
                .createQuery("select max(inventoryNumber) from Computer").uniqueResult();
        if(maxNumber == null) {
            return 0;
        }
        return maxNumber.intValue();
    }

    @Override
    public int getComputerCount() {
        return ((Number)sessionFactory.getCurrentSession().createCriteria(Computer.class)
                .setProjection(Projections.rowCount()).uniqueResult())
                .intValue();
    }

    @Override
    public List<Computer> computerList() {
        return sessionFactory.getCurrentSession().createCriteria(Computer.class).list();
    }

    @Override
    public void updateComputer(Computer computer) {
        sessionFactory.getCurrentSession().update(computer);
    }

    @Override
    public void removeComputer(int inventoryNumber) {
        Computer computer = (Computer) sessionFactory.getCurrentSession().get(Computer.class, inventoryNumber);
        if(computer != null)
            sessionFactory.getCurrentSession().delete(computer);
    }
}
