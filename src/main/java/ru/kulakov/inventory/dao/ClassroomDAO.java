package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.Classroom;

import java.util.List;

public interface ClassroomDAO {
    public void addClassroom(Classroom classroom);
    public Classroom getClassroom(int number);
    public List<Classroom> getClassroomList();
    public void deleteClassroom(int number);

    int[] getClassroomSize(int number);
}
