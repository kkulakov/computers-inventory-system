package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.EquipmentHistory;

import java.util.List;

public interface EquipmentHistoryDAO {
    public void addEquipmentToHistory(EquipmentHistory equipmentHistory);
    public List<EquipmentHistory> getHistoryList(int id);
    public int getHistorySizeForEquipment(int id);
    public EquipmentHistory getFirstEquipmentFromHistory();
    public void deleteEquipmentFromHistory(EquipmentHistory equipmentHistory);
}
