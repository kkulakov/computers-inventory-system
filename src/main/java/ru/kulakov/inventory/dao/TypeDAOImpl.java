package ru.kulakov.inventory.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kulakov.inventory.domain.Type;

import java.util.List;

@Repository
public class TypeDAOImpl implements TypeDAO {
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public void addType(Type type) {
        sessionFactory.getCurrentSession().save(type);
    }

    @Override
    public Type getType(int code) {
        return (Type) sessionFactory.getCurrentSession().get(Type.class, code);
    }

    @Override
    public List<Type> typeList() {
        return sessionFactory.getCurrentSession().createCriteria(Type.class).list();
    }
}
