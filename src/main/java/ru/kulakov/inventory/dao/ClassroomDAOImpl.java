package ru.kulakov.inventory.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kulakov.inventory.domain.Classroom;

import java.util.List;

@Repository
public class ClassroomDAOImpl implements ClassroomDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addClassroom(Classroom classroom) {
        sessionFactory.getCurrentSession().save(classroom);
    }

    @Override
    public Classroom getClassroom(int number) {
        return (Classroom) sessionFactory.getCurrentSession().get(Classroom.class, number);
    }

    @Override
    public List<Classroom> getClassroomList() {
        return sessionFactory.getCurrentSession().createCriteria(Classroom.class).list();
    }

    @Override
    public int[] getClassroomSize(int number) {
        Classroom classroom = (Classroom) sessionFactory.getCurrentSession().get(Classroom.class, number);
        if(classroom != null) {
            return new int[]{classroom.getHorizontalSize(), classroom.getVerticalSize()};
        }
        return new int[]{0, 0};
    }

    @Override
    public void deleteClassroom(int number) {
        Classroom classroom = (Classroom) sessionFactory.getCurrentSession().get(Classroom.class, number);
        if(classroom != null) {
            sessionFactory.getCurrentSession().delete(classroom);
        }
    }
}
