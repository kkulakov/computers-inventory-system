package ru.kulakov.inventory.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kulakov.inventory.domain.ComputerHistory;

import java.util.List;

@Repository
public class ComputerHistoryDAOImpl implements ComputerHistoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addComputerToHistory(ComputerHistory computerHistory) {
        sessionFactory.getCurrentSession().save(computerHistory);
    }

    @Override
    public List<ComputerHistory> getHistoryList(int id) {
        String hql = "from ComputerHistory where inventoryNumber = :id";
        return sessionFactory.getCurrentSession().createQuery(hql).setInteger("id", id).list();
    }

    @Override
    public int getHistorySizeForComputer(int id) {
        String hql = "select count(*) from ComputerHistory where inventoryNumber = :id";
        Number historySize = (Number)sessionFactory.getCurrentSession()
                .createQuery(hql).setInteger("id", id).uniqueResult();
        if(historySize == null){
            return 0;
        }
        return historySize.intValue();
    }

    @Override
    public ComputerHistory getFirstComputerFromHistory() {
        DetachedCriteria minId = DetachedCriteria.forClass(ComputerHistory.class).setProjection(Projections.min("id"));
        return (ComputerHistory) sessionFactory.getCurrentSession()
                .createCriteria(ComputerHistory.class)
                .add( Property.forName("id").eq(minId) )
                .uniqueResult();
    }

    @Override
    public void deleteComputerFromHistory(ComputerHistory computerHistory) {
        if(computerHistory != null){
            sessionFactory.getCurrentSession().delete(computerHistory);
        }
    }
}
