package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.Equipment;

import java.util.List;

public interface EquipmentDAO {
    public void addEquipment(Equipment equipment);
    public Equipment getEquipment(int inventoryNumber);
    public int getNextInventoryNumber();
    public int getEquipmentCount();
    public List<Equipment> equipmentList();
    public void updateEquipment(Equipment equipment);
    public void removeEquipment(int inventoryNumber);
}
