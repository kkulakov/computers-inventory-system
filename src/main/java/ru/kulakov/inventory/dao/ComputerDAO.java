package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.Computer;

import java.util.List;

public interface ComputerDAO {
    public void addComputer(Computer computer);
    public Computer getComputer(int inventoryNumber);
    public int getNextInventoryNumber();
    public int getComputerCount();
    public List<Computer> computerList();
    public void updateComputer(Computer computer);
    public void removeComputer(int inventoryNumber);
}
