package ru.kulakov.inventory.dao;

import ru.kulakov.inventory.domain.ComputerHistory;

import java.util.List;

public interface ComputerHistoryDAO {
    public void addComputerToHistory(ComputerHistory computerHistory);
    public List<ComputerHistory> getHistoryList(int id);
    public int getHistorySizeForComputer(int id);
    public ComputerHistory getFirstComputerFromHistory();
    public void deleteComputerFromHistory(ComputerHistory computerHistory);
}
