package ru.kulakov.inventory.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kulakov.inventory.domain.Equipment;

import java.util.List;

@Repository
public class EquipmentDAOImpl implements EquipmentDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addEquipment(Equipment equipment) {
        sessionFactory.getCurrentSession().save(equipment);
    }

    @Override
    public Equipment getEquipment(int inventoryNumber) {
        return (Equipment) sessionFactory.getCurrentSession().get(Equipment.class, inventoryNumber);
    }

    @Override
    public int getNextInventoryNumber() {
        Number maxNumber = (Number) sessionFactory.getCurrentSession()
                .createQuery("select max(inventoryNumber) from Equipment").uniqueResult();
        if(maxNumber == null) {
            return 0;
        }
        return maxNumber.intValue();
    }

    @Override
    public int getEquipmentCount() {
        return ((Number)sessionFactory.getCurrentSession().createCriteria(Equipment.class)
                .setProjection(Projections.rowCount()).uniqueResult())
                .intValue();
    }

    @Override
    public List<Equipment> equipmentList() {
        return sessionFactory.getCurrentSession().createCriteria(Equipment.class).list();
    }

    @Override
    public void updateEquipment(Equipment equipment) {
        sessionFactory.getCurrentSession().update(equipment);
    }

    @Override
    public void removeEquipment(int inventoryNumber) {
        Equipment equipment = (Equipment) sessionFactory.getCurrentSession().get(Equipment.class, inventoryNumber);
        if(equipment != null)
            sessionFactory.getCurrentSession().delete(equipment);
    }
}
