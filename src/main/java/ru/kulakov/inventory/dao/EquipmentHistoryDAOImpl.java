package ru.kulakov.inventory.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kulakov.inventory.domain.EquipmentHistory;

import java.util.List;

@Repository
public class EquipmentHistoryDAOImpl implements EquipmentHistoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addEquipmentToHistory(EquipmentHistory equipmentHistory) {
        sessionFactory.getCurrentSession().save(equipmentHistory);
    }

    @Override
    public List<EquipmentHistory> getHistoryList(int id) {
        String hql = "from EquipmentHistory where inventoryNumber = :id";
        return sessionFactory.getCurrentSession().createQuery(hql).setInteger("id", id).list();
    }

    @Override
    public int getHistorySizeForEquipment(int id) {
        String hql = "select count(*) from EquipmentHistory where inventoryNumber = :id";
        Number historySize = (Number) sessionFactory.getCurrentSession()
                .createQuery(hql).setInteger("id", id).uniqueResult();
        if(historySize == null){
            return 0;
        }
        return historySize.intValue();
    }

    @Override
    public EquipmentHistory getFirstEquipmentFromHistory() {
        DetachedCriteria minId = DetachedCriteria.forClass(EquipmentHistory.class).setProjection(Projections.min("id"));
        return (EquipmentHistory) sessionFactory.getCurrentSession()
                .createCriteria(EquipmentHistory.class)
                .add( Property.forName("id").eq(minId) )
                .uniqueResult();
    }

    @Override
    public void deleteEquipmentFromHistory(EquipmentHistory equipmentHistory) {
        if(equipmentHistory != null){
            sessionFactory.getCurrentSession().delete(equipmentHistory);
        }
    }
}
