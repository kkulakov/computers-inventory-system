package ru.kulakov.inventory.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kulakov.inventory.domain.Classroom;
import ru.kulakov.inventory.domain.Computer;
import ru.kulakov.inventory.domain.Equipment;
import ru.kulakov.inventory.service.ClassroomService;
import ru.kulakov.inventory.service.ComputerService;
import ru.kulakov.inventory.service.EquipmentService;
import ru.kulakov.inventory.service.TypeService;

import javax.annotation.PostConstruct;
import java.util.*;

@Controller
public class MainController {
    @Autowired
    ComputerService computerService;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
    TypeService typeService;
    @Autowired
    ClassroomService classroomService;

    private final Comparator<Computer> computerComparatorByClassroom = new Comparator<Computer>() {
        @Override
        public int compare(Computer c1, Computer c2) {
            return Integer.compare(c1.getClassroom().getNumber(), c2.getClassroom().getNumber());
        }
    };

    @RequestMapping(value={"/", "/show", "/show/computer", "/show/equipment"})
    public String home() {
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String mainPage(Map<String, Object> map) {

        map.put("computerCount", computerService.getComputerCount());
        map.put("equipmentCount", equipmentService.getEquipmentCount());

        return "index";
    }


    @RequestMapping("/browse")
    public String browsePage(Map<String, Object> map) {
        List<Computer> computerList = computerService.computerList();
        Collections.sort(computerList, computerComparatorByClassroom);
        map.put("computerList", computerList);
        map.put("equipmentList", equipmentService.equipmentList());
        return "browse";
    }

    @RequestMapping("/show/getlastid")
    public @ResponseBody String getLastId() {
        int computerInventoryNumber = computerService.getNextInventoryNumber() + 1;
        int equipmentInventoryNumber = equipmentService.getNextInventoryNumber() + 1;
        Integer nextInventoryNumber = (computerInventoryNumber > equipmentInventoryNumber) ?
                computerInventoryNumber : equipmentInventoryNumber;
        return nextInventoryNumber.toString();
    }

}
