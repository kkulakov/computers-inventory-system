package ru.kulakov.inventory.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kulakov.inventory.domain.Classroom;
import ru.kulakov.inventory.service.ClassroomService;

import java.util.Map;

@Controller
public class ClassroomController {
    @Autowired
    ClassroomService classroomService;

    @RequestMapping("/add/classroom")
    public String addClassroom(Map<String, Object> map) {
        map.put("classroom", new Classroom());
        return "addClassroom";
    }

    @RequestMapping(value = "/add/classroom", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Classroom addClassroom(@RequestBody Classroom classroom){
        classroomService.addClassroom(classroom);

        return classroom;
    }
}
