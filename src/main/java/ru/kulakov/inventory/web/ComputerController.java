package ru.kulakov.inventory.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kulakov.inventory.domain.Computer;
import ru.kulakov.inventory.service.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
public class ComputerController {
    @Autowired
    ComputerService computerService;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
    TypeService typeService;
    @Autowired
    ClassroomService classroomService;
    @Autowired
    ComputerHistoryService computerHistoryService;


    @RequestMapping("/add/computer")
    public String addPage(Map<String, Object> map) {
        map.put("computer", new Computer());
        map.put("classroomList", classroomService.getClassroomList());
        int nextComputerNumber = computerService.getNextInventoryNumber() + 1;
        int nextEquipmentNumber = equipmentService.getNextInventoryNumber() + 1;
        map.put("nextInventoryNumber", (nextComputerNumber > nextEquipmentNumber) ?
                nextComputerNumber : nextEquipmentNumber);
        return "addComputer";
    }

    @RequestMapping(value = "/add/computer", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Computer addComputer(@RequestBody Computer computer) {

        computerService.addComputer(computer);

        return computer;
    }

    @RequestMapping(value = "/getComputerList", method = RequestMethod.GET)
    public @ResponseBody List<Computer> getComputerList() {
        return computerService.computerList();
    }

    @RequestMapping("/show/computer/{id}")
    public String showComputer(@PathVariable int id, Map<String, Object> map) {
        Computer computer = computerService.getComputer(id);
        map.put("computer", computer);
        map.put("historyList", computerHistoryService.getHistoryList(id));
        map.put("classroomList", classroomService.getClassroomList());
        return (computer != null) ? "showComputer" : "redirect:/browse";
    }

    @RequestMapping(value = "/show/computer/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Computer editComputer(@PathVariable int id, @RequestBody Computer computer) {
        Computer oldComputer = computerService.getComputer(id);
        if(null == oldComputer) {
            return null;
        }

        computerHistoryService.addComputerToHistory(oldComputer);
        computer.setInventoryNumber(id);
        return computerService.updateComputer(computer);
    }
}
