package ru.kulakov.inventory.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kulakov.inventory.domain.Computer;
import ru.kulakov.inventory.domain.Equipment;
import ru.kulakov.inventory.service.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Controller
public class EquipmentController {
    @Autowired
    ComputerService computerService;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
    TypeService typeService;
    @Autowired
    ClassroomService classroomService;
    @Autowired
    ComputerHistoryService computerHistoryService;
    @Autowired
    EquipmentHistoryService equipmentHistoryService;

    private final Comparator<Computer> computerComparatorByClassroom = new Comparator<Computer>() {
        @Override
        public int compare(Computer c1, Computer c2) {
            return Integer.compare(c1.getClassroom().getNumber(), c2.getClassroom().getNumber());
        }
    };

    @RequestMapping("/add/equipment")
    public String addPage(Map<String, Object> map) {
        List<Computer> computerList = computerService.computerList();
        Collections.sort(computerList, computerComparatorByClassroom);
        map.put("equipment", new Equipment());
        map.put("typeList", typeService.typeList());
        map.put("computerList", computerList);
        map.put("classroomList", classroomService.getClassroomList());
        int nextComputerNumber = computerService.getNextInventoryNumber() + 1;
        int nextEquipmentNumber = equipmentService.getNextInventoryNumber() + 1;
        map.put("nextInventoryNumber", (nextComputerNumber > nextEquipmentNumber) ?
                nextComputerNumber : nextEquipmentNumber);
        return "addEquipment";
    }

    @RequestMapping(value = "/add/equipment", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Equipment addEquipment(@RequestBody Equipment equipment) {

        equipmentService.addEquipment(equipment);

        return equipment;
    }

    @RequestMapping(value = "/getEquipmentList", method = RequestMethod.GET)
    public @ResponseBody
    List<Equipment> getEquipmentList() {
        return equipmentService.equipmentList();
    }

    @RequestMapping("/show/equipment/{id}")
    public String showEquipment(@PathVariable int id, Map<String, Object> map) {
        Equipment equipment = equipmentService.getEquipment(id);
        map.put("equipment", equipment);
        map.put("computerList", computerService.computerList());
        map.put("typeList", typeService.typeList());
        map.put("classroomList", classroomService.getClassroomList());
        map.put("historyList", equipmentHistoryService.getHistoryList(id));
        return (equipment != null) ? "showEquipment" : "redirect:/browse";
    }

    @RequestMapping(value = "/show/equipment/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Equipment editEquipment(@PathVariable int id, @RequestBody Equipment equipment) {
        Equipment oldEquipment = equipmentService.getEquipment(id);
        if(null == oldEquipment){
            return null;
        }

        equipmentHistoryService.addEquipmentToHistory(oldEquipment);
        equipment.setInventoryNumber(id);
        return equipmentService.updateEquipment(equipment);
    }
}
