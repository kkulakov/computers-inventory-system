<%--@elvariable id="computerCount" type="int"--%>
<%--@elvariable id="equipmentCount" type="int"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <title>Система інвентарізації</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <br>
        <br>
        <div align="center">
            <div>Всього комп'ютерів: ${computerCount}</div>
            <div>Всього обладнання: ${equipmentCount}</div>
        </div>
    </body>
</html>