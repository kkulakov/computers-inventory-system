<%--@elvariable id="typeList" type="java.util.List"--%>
<%--@elvariable id="computerList" type="java.util.List"--%>
<%--@elvariable id="classroomList" type="java.util.List"--%>
<%--@elvariable id="nextInventoryNumber" type="int"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Додати обладнання</title>
        <link rel="stylesheet" href="/resources/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <script src="/resources/js/jquery-1.10.2.js"></script>
        <script src="/resources/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="/resources/js/custom/addComputer.js"></script>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <br>
        <br>
        <br>
        <div align="center">
            <form:form method="post" action="/add/equipment" commandName="equipment" id="equipmentForm">
                <table>
                    <tr>
                        <td><form:label path="inventoryNumber">
                            Інвентарний номер
                        </form:label></td>
                        <td><form:input path="inventoryNumber" value="${nextInventoryNumber}" class="equipmentInventoryNumber"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="type">
                            Тип
                        </form:label></td>
                        <td>
                            <form:select path="type">
                                <form:options items="${typeList}" itemLabel="typeName" itemValue="code"/>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td><form:label path="model">
                            Модель
                        </form:label></td>
                        <td><form:input path="model" /></td>
                    </tr>
                    <tr>
                        <td><form:label path="description">
                            Опис
                        </form:label></td>
                        <td><form:input path="description" /></td>
                    </tr>
                    <tr>
                        <td><form:label path="classroom">
                            Аудиторія
                        </form:label></td>
                        <td>
                            <form:select path="classroom" class="equipment_classroom">
                                <form:options items="${classroomList}" itemLabel="number" itemValue="number" />
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td><form:label path="computer">
                            ІН комп'ютера
                        </form:label></td>
                        <td>
                            <form:select path="computer">
                                <c:forEach items="${computerList}" var="computer">
                                    <form:option value="${computer.inventoryNumber}"
                                                 label="k${computer.classroom.number}: ${computer.inventoryNumber}"
                                                 id="${computer.classroom.number}"/>
                                </c:forEach>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="Додати" /></td>
                    </tr>
                </table>
            </form:form>
            <div id = "success" class="ui-widget-content ui-corner-all">
                <br>
                <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
                <p>
                    Дані успішно збережені.
                </p>
            </div>
            <div id = "fail" class="ui-widget-content ui-corner-all">
                <br>
                <h3 class="ui-widget-header ui-corner-all">Помилка! :(</h3>
                <p>
                    Дані не були збережені.
                </p>
            </div>
        </div>
    </body>
</html>