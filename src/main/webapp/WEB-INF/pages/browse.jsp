<%--@elvariable id="computerList" type="java.util.List"--%>
<%--@elvariable id="equipmentList" type="java.util.List"--%>
<%--@elvariable id="computer" type="ru.kulakov.inventory.domain.Computer"--%>
<%--@elvariable id="equipment" type="ru.kulakov.inventory.domain.Equipment"--%>
<%--@elvariable id="prevComputer" type="ru.kulakov.inventory.domain.Computer"--%>
<%--@elvariable id="equipmentPresent" type="boolean"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Перегляд</title>
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <script src="/resources/js/jquery-1.10.2.js"></script>
        <script src="/resources/js/custom/browse.js"></script>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <br>
        Оберіть тип відображення:
        <br>
        <input type="radio" name="viewSwitch" value="computer" checked> Комп'ютери
        <input type="radio" name="viewSwitch" value="equipment"> Обладнання
        <input type="radio" name="viewSwitch" value="combined"> Сортування по кабінетам
        <br>
        <div id="computers">
            <h3>Комп'ютери:</h3>
            <table border="1">
                <tr>
                    <th>Інвентарний номер
                    <th>Процесор
                    <th>Материнська плата
                    <th>Оперативна пам'ять
                    <th>Жорсткий диск
                    <th>Аудиторія
                    <th>Інше
                    <th>Дата додання
                </tr>
                <c:forEach items="${computerList}" var="computer">
                    <tr>
                        <td><a href="/show/computer/${computer.inventoryNumber}">${computer.inventoryNumber}</a></td>
                        <td>${computer.processor}</td>
                        <td>${computer.motherboard}</td>
                        <td>${computer.ram}</td>
                        <td>${computer.hdd}</td>
                        <td>${computer.classroom.number}</td>
                        <td>${computer.other}</td>
                        <td><fmt:formatDate value="${computer.addingTime}" pattern="dd.MM.yy HH:mm" /></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div id="equipment" style="display: none;">
            <h3>Обладнання:</h3>
            <table border="1">
                <tr>
                    <th>Інвентарний номер
                    <th>Тип
                    <th>Модель
                    <th>Опис
                    <th>ІН комп'ютера
                    <th>Аудиторія
                    <th>Дата додання
                </tr>
                <c:forEach items="${equipmentList}" var="equipment">
                    <tr>
                        <td><a href="/show/equipment/${equipment.inventoryNumber}">${equipment.inventoryNumber}</a></td>
                        <td>${equipment.type.typeName}</td>
                        <td>${equipment.model}</td>
                        <td>${equipment.description}</td>
                        <td>${equipment.computer.inventoryNumber}</td>
                        <td>${equipment.classroom.number}</td>
                        <td><fmt:formatDate value="${equipment.addingTime}" pattern="dd.MM.yy HH:mm" /></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div id="combined" style="display: none;">
            <c:forEach items="${computerList}" var="computer">
                <c:if test="${empty prevComputer or computer.classroom.number != prevComputer.classroom.number}">
                    <br>
                    <div align="center"><h3>Кабінет ${computer.classroom.number}</h3></div>
                    <br>
                </c:if>
                <table>
                    <tr valign="top">
                        <td>
                            <c:set var="prevComputer" value="${computer}" />
                            <table border="1">
                                <tr>
                                    <th>Інвентарний номер</th>
                                    <td width="200px">
                                        <a href="/show/computer/${computer.inventoryNumber}">
                                            ${computer.inventoryNumber}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Процесор</th>
                                    <td>${computer.processor}</td>
                                </tr>
                                <tr>
                                    <th>Материнська плата</th>
                                    <td>${computer.motherboard}</td>
                                </tr>
                                <tr>
                                    <th>Оперативна пам'ять</th>
                                    <td>${computer.ram}</td>
                                </tr>
                                <tr>
                                    <th>Жорсткий диск</th>
                                    <td>${computer.hdd}</td>
                                </tr>
                                <tr>
                                    <th>Аудиторія</th>
                                    <td>${computer.classroom.number}</td>
                                </tr>
                                <tr>
                                    <th>Інше</th>
                                    <td>${computer.other}</td>
                                </tr>
                                <tr>
                                    <th>Дата додання</th>
                                    <td><fmt:formatDate value="${computer.addingTime}" pattern="dd.MM.yy HH:mm" /></td>
                                </tr>
                            </table>

                        </td>
                        <td>
                            <table border="1">
                                <c:set var="equipmentPresent" value="${true}" />
                                <c:forEach items="${equipmentList}" var="equipment">
                                    <c:if test="${equipmentPresent and computer.inventoryNumber == equipment.computer.inventoryNumber}">
                                            <c:set var="equipmentPresent" value="${false}" />
                                            <tr>
                                                <th>Тип
                                                <th>Інвентарний номер
                                                <th>Модель
                                                <th>Опис
                                                <th>ІН комп'ютера
                                                <th>Аудиторія
                                                <th>Дата додання
                                            </tr>
                                    </c:if>
                                        <c:if test="${computer.inventoryNumber == equipment.computer.inventoryNumber}">
                                            <tr>
                                                <th>${equipment.type.typeName}</th>
                                                <td><a href="/show/equipment/${equipment.inventoryNumber}">${equipment.inventoryNumber}</a></td>
                                                <td>${equipment.model}</td>
                                                <td>${equipment.description}</td>
                                                <td>${equipment.computer.inventoryNumber}</td>
                                                <td>${equipment.classroom.number}</td>
                                                <td><fmt:formatDate value="${equipment.addingTime}" pattern="dd.MM.yy HH:mm" /></td>
                                            </tr>
                                        </c:if>
                                </c:forEach>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <hr width="600">
                <br>
            </c:forEach>
        </div>
    </body>
</html>