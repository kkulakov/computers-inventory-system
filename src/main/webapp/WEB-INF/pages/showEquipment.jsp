<%--@elvariable id="equipment" type="ru.kulakov.inventory.domain.Equipment"--%>
<%--@elvariable id="computerList" type="java.util.List"--%>
<%--@elvariable id="typeList" type="java.util.List"--%>
<%--@elvariable id="classroomList" type="java.util.List"--%>
<%--@elvariable id="historyList" type="java.util.List"--%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>${equipment.type.typeName}: ${equipment.inventoryNumber}</title>
        <link rel="stylesheet" href="/resources/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <script src="/resources/js/jquery-1.10.2.js"></script>
        <script src="/resources/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="/resources/js/custom/showEquipment.js"></script>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <br>
        <br>
        <br>
        <br>
        <div align = "center">
        <form:form method="PUT" id = "editEquipmentForm" commandName="equipment"
                   action="${pageContext.request.contextPath}/show/equipment/${equipment.inventoryNumber}">
            <table>
                <tr>
                    <td>Інвентарний номер:
                    <td><form:input path="inventoryNumber" type="text" disabled = "true"
                                    value="${equipment.inventoryNumber}"/>
                </tr>
                <tr>
                    <td><form:label path="type">
                        Тип
                    </form:label></td>
                    <td>
                        <form:select path="type">
                            <form:options items="${typeList}" itemLabel="typeName" itemValue="code"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><form:label path="model">
                        Модель
                    </form:label></td>
                    <td><form:input path="model" /></td>
                </tr>
                <tr>
                    <td><form:label path="description">
                        Опис
                    </form:label></td>
                    <td><form:input path="description" /></td>
                </tr>
                <tr>
                    <td><form:label path="classroom">
                        Аудиторія
                    </form:label></td>
                    <td>
                        <form:select path="classroom" class="classroom">
                            <form:options items="${classroomList}" itemLabel="number" itemValue="number" />
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td><form:label path="computer">
                        ІН комп'ютера
                    </form:label></td>
                    <td>
                        <form:select path="computer">
                            <c:forEach items="${computerList}" var="computer">
                                <form:option value="${computer.inventoryNumber}"
                                             label="k${computer.classroom.number}: ${computer.inventoryNumber}"
                                             id="${computer.classroom.number}"/>
                            </c:forEach>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td>
                    <td><input type="submit" value="Зберегти"/>
                </tr>
            </table>
        </form:form>
        <br>
        <br>
        <button onclick="duplicateEntry()">Дублювати запис</button>
        <input type="text" id="duplicateInventoryNumber" value="0">
        <div id="dialog-confirm" title="&nbsp&nbspДублювати запис?" style="display: none; background: LightSalmon;">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Дублювання запису призведе до запису до БД аналогічного &nbsp&nbsp&nbsp&nbsp&nbspпристрою, але з іншим &nbsp&nbsp&nbsp&nbsp&nbspінвентарним номером. Це саме &nbsp&nbsp&nbsp&nbsp&nbspте, чого ви хочете?<br><br></p>
        </div>
        <div id = "success" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
            <p>
                Дані про ${equipment.type.typeName} ${equipment.inventoryNumber} успішно оновлено.
            </p>
        </div>
        <div id = "successDuplicated" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
            <p>
                ${equipment.type.typeName} ${equipment.inventoryNumber} успішно дублікован.
            </p>
        </div>
        <div id = "fail" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Помилка! :(</h3>
            <p>
                Дані не були збережені.
            </p>
        </div>
        <c:if test="${!empty historyList}">
            <br>
            <br>
            <br>
            <h3>Історія змін обладнання:</h3>
            <table border="1">
                <tr>
                    <th>Тип
                    <th>Модель
                    <th>Опис
                    <th>ІН комп'ютера
                    <th>Аудиторія
                    <th>Дата додання
                    <th>
                </tr>
                <c:forEach items="${historyList}" var="history">
                    <tr>
                        <td>${history.type.typeName}</td>
                        <td>${history.model}</td>
                        <td>${history.description}</td>
                        <td>${history.computer.inventoryNumber}</td>
                        <td>${history.classroom.number}</td>
                        <td><fmt:formatDate value="${history.writingTime}" pattern="dd.MM.yy HH:mm" /></td>
                        <td>
                            <input type="button" value="Відновити" onclick="updateEquipment('${history.type.typeName}',
                                    '${history.model}', '${history.description}', '${history.computer.inventoryNumber}',
                                    '${history.classroom.number}')">
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        </div>
    </body>
</html>