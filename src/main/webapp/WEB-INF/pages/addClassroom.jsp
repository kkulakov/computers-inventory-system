<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Додати кабінет</title>
        <link rel="stylesheet" href="/resources/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
        <link rel="stylesheet" href="/resources/css/custom/general.css">
        <script src="/resources/js/jquery-1.10.2.js"></script>
        <script src="/resources/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="/resources/js/custom/addClassroom.js"></script>
    </head>
    <body>
        <%@include file="menu.jsp" %>
        <br>
        <br>
        <div align = "center">
            <form:form method="post" action="/add/classroom" commandName="classroom" id="classroomForm">
                <table>
                    <tr>
                        <td>
                            <form:label path="number">
                            Номер кабінету:
                            </form:label>
                        </td>
                        <td>
                            <form:input path="number" autofocus="1"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="verticalSize">
                           Довжина кабінету:
                            </form:label>
                        </td>
                        <td>
                            <form:input path="verticalSize"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="horizontalSize">
                            Ширина кабінету:
                            </form:label>
                        </td>
                        <td>
                            <form:input path="horizontalSize"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="Додати" /></td>
                    </tr>
                 </table>
                 <div id="classroomSizePicker"></div>
            </form:form>
            <div id = "success" class="ui-widget-content ui-corner-all">
                <br>
                <h3 class="ui-widget-header ui-corner-all">Успіх!</h3>
                <p>
                    Дані успішно збережені.
                </p>
            </div>
            <div id = "fail" class="ui-widget-content ui-corner-all">
            <br>
            <h3 class="ui-widget-header ui-corner-all">Помилка! :(</h3>
            <p>
                Дані не були збережені.
            </p>
        </div>
        </div>
    </body>
</html>