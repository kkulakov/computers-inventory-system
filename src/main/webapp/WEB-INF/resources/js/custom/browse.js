$(document).ready(function() {
    $('input[type=radio][name=viewSwitch]').change(function() {
        if (this.value == 'computer') {
            $('#equipment').css('display', 'none');
            $('#combined').css('display', 'none');
            $('#computers').css('display', 'block');
        }
        else if (this.value == 'equipment') {
            $('#computers').css('display', 'none');
            $('#combined').css('display', 'none');
            $('#equipment').css('display', 'block');
        }
        else if (this.value == 'combined') {
            $('#computers').css('display', 'none');
            $('#equipment').css('display', 'none');
            $('#combined').css('display', 'block');

        }
    });
});