$(document).ready(function() {

    //making autocompletion
    var model = [];
    var description = [];
    $.get( "/getEquipmentList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            model.push(data[i].model);
            description.push(data[i].description);
        }
        $( "#model" ).autocomplete({source: model});
        $( "#description" ).autocomplete({source: description});
    });


    var computerSelect = $('#computer');
    var allComputersOptions = computerSelect.children();
    $('.equipment_classroom').change(function() {
        var classroom = $('.equipment_classroom').val();

        allComputersOptions.detach().each(function(){
            var val = $(this).attr('id');
            if (val == '' || val == classroom) {
                $(this).appendTo(computerSelect);
            }
        });
        computerSelect.val('');
        if(computerSelect.has('option').length == 0){
            allComputersOptions.each(function() {
                $(this).appendTo(computerSelect);
            })
        }
        $("#computer").find(":first").attr("selected", "selected");
    });

    //sending forms via AJAX
    $('#equipmentForm').submit(function(event) {

        var inventoryNumber = $('.equipmentInventoryNumber').val();
        var type = $('#type').val();
        var model = $('#model').val();
        var description = $('#description').val();
        var classroom = $('.equipment_classroom').val();
        var computer = $('#computer').val();
        var json = {
            "inventoryNumber": inventoryNumber,
            "type": {"code" : type},
            "model": model,
            "description": description,
            "classroom": {"number": classroom},
            "computer": {inventoryNumber: computer}};

        $.ajax({
            url: $("#equipmentForm").attr( "action"),
            data: JSON.stringify(json),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(equipment) {
                if (equipment != null && equipment != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });
});

function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }

}
