$(document).ready(function() {
    $( "#success" ).hide();
    $( "#successDuplicated" ).hide();
    $( "#fail" ).hide();


    var processor = [];
    var motherboard = [];
    var ram = [];
    var hdd = [];
    $.get( "/getComputerList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            processor.push(data[i].processor);
            motherboard.push(data[i].motherboard);
            ram.push(data[i].ram);
            hdd.push(data[i].hdd);
        }
        $( "#processor" ).autocomplete({source: processor});
        $( "#motherboard" ).autocomplete({source: motherboard});
        $( "#ram" ).autocomplete({source: ram});
        $( "#hdd" ).autocomplete({source: hdd});
    });


    $('#editComputerForm').submit(function(event) {

        var processor = $('#processor').val();
        var motherboard = $('#motherboard').val();
        var ram = $('#ram').val();
        var hdd = $('#hdd').val();
        var classroom = $('#classroom').val();
        var other = $('#other').val();
        var json = {
            "processor" : processor,
            "motherboard" : motherboard,
            "ram": ram,
            "hdd": hdd,
            "classroom": {"number": classroom},
            "other": other
        };

        $.ajax({
            url: $("#editComputerForm").attr( "action"),
            data: JSON.stringify(json),
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(computer) {
                if (computer != null && computer != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });

    //setting last inventory number for duplicate function
    $.get( "/show/getlastid", function( data ) {
        $( '#duplicateInventoryNumber' ).val(data);
    });
});
function updateComputer(processor, motherboard, ram, hdd, classroom, other){
    var json = {
        "processor" : processor,
        "motherboard" : motherboard,
        "ram": ram,
        "hdd": hdd,
        "classroom": {"number": classroom},
        "other": other
    };

    $.ajax({
        url: $("#editComputerForm").attr( "action"),
        data: JSON.stringify(json),
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(computer) {
            if (computer != null && computer != undefined)
                location.reload(); //this line is different
            else
                showBlock("#fail");
        },
        error: function () {
            showBlock("#fail");
        }
    });
}

function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }
}

function duplicateEntry() {
    $( "#dialog-confirm" ).dialog({
        dialogClass: "no-close",
        resizable: false,
        height:225,
        modal: true,
        buttons: {
            "Дублювати": function() {
                var inventoryNumber = $('#duplicateInventoryNumber').val();
                var processor = $('#processor').val();
                var motherboard = $('#motherboard').val();
                var ram = $('#ram').val();
                var hdd = $('#hdd').val();
                var classroom = $('#classroom').val();
                var other = $('#other').val();
                var json = {
                    "inventoryNumber" : inventoryNumber,
                    "processor" : processor,
                    "motherboard" : motherboard,
                    "ram": ram,
                    "hdd": hdd,
                    "classroom": {"number": classroom},
                    "other": other
                };

                $.ajax({
                    url: "/add/computer",
                    data: JSON.stringify(json),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(computer) {
                        if (computer != null && computer != undefined) {
                            showBlock("#successDuplicated");
                            $('#duplicateInventoryNumber').val(parseInt($('#duplicateInventoryNumber').val()) + 1);
                        }
                        else
                            showBlock("#fail");
                    },
                    error: function () {
                        showBlock("#fail");
                    }
                });
                $( this ).dialog( "close" );
            },
            "Відмінити": function() {
                $( this ).dialog( "close" );
            }
        }
    });
}