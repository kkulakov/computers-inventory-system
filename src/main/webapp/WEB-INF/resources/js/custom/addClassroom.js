var MAX_CLASSROOM_PICKER_VSIZE = 15;
var MAX_CLASSROOM_PICKER_HSIZE = 20;
var fixClassroomSizeChoose = false;

$(document).ready(function() {

    $( "#success" ).hide();
    $( "#fail" ).hide();

    fillClassroomSizePicker();

    $('#classroomForm').submit(function(event) {

        var number = $('#number').val();
        var verticalSize = $('#verticalSize').val();
        var horizontalSize = $('#horizontalSize').val();
        var json = {
            "number" : number,
            "verticalSize" : verticalSize,
            "horizontalSize" : horizontalSize
        };

        $.ajax({
            url: $("#classroomForm").attr( "action"),
            data: JSON.stringify(json),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(classroom) {
                if (classroom != null && classroom != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });
});

function fillClassroomSizePicker () {
    var target = $('#classroomSizePicker');
    var counter = 0;
    for(var y = 0; y < MAX_CLASSROOM_PICKER_VSIZE; y++){
        for(var x = 0; x < MAX_CLASSROOM_PICKER_HSIZE; x++){
            var $div = $("<div>", {id: counter++, class: "tile"});

            $div.mouseover(function() {
                tileOver(parseInt(this.id));
            });
            $div.mousedown(function() {
                fixClassroomSizeChoose = !fixClassroomSizeChoose;
            });
            target.append($div);
            target.append(" ");
        }
        target.append("<br>");
    }

}

function tileOver(id){
    if(fixClassroomSizeChoose) return;
    var counter = 0;
    var x = id % MAX_CLASSROOM_PICKER_HSIZE;
    var y = Math.floor((id - x) / MAX_CLASSROOM_PICKER_HSIZE);
    for(var i = 0; i < MAX_CLASSROOM_PICKER_VSIZE; i++){
        for(var j = 0; j < MAX_CLASSROOM_PICKER_HSIZE; j++){
            if(i <= y && j <= x) {
                $('#' + counter).css("background" , "blue");
            }
            else {
                $('#' + counter).css("background" , "red");
            }
            counter++;
        }

    }
    $('#horizontalSize').val(x + 1);
    $('#verticalSize').val(y + 1);
}
function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }

}