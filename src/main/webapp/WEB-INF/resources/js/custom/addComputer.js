$(document).ready(function() {
    //making autocompletion
    var processor = [];
    var motherboard = [];
    var ram = [];
    var hdd = [];
    $.get( "/getComputerList", function( data ) {
        for (var i = 0; i < data.length; i++) {
            processor.push(data[i].processor);
            motherboard.push(data[i].motherboard);
            ram.push(data[i].ram);
            hdd.push(data[i].hdd);
        }
        $( "#processor" ).autocomplete({source: processor});
        $( "#motherboard" ).autocomplete({source: motherboard});
        $( "#ram" ).autocomplete({source: ram});
        $( "#hdd" ).autocomplete({source: hdd});
    });

    $( "#success" ).hide();
    $( "#fail" ).hide();

    //sending forms via AJAX
    $('#computerForm').submit(function(event) {

        var inventoryNumber = $('.computerInventoryNumber').val();
        var processor = $('#processor').val();
        var motherboard = $('#motherboard').val();
        var ram = $('#ram').val();
        var hdd = $('#hdd').val();
        var classroom = $('#classroom').val();
        var other = $('#other').val();
        var json = {
            "inventoryNumber" : inventoryNumber,
            "processor" : processor,
            "motherboard" : motherboard,
            "ram": ram,
            "hdd": hdd,
            "classroom": {"number": classroom},
            "other": other
        };

        $.ajax({
            url: $("#computerForm").attr( "action"),
            data: JSON.stringify(json),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(computer) {
                if (computer != null && computer != undefined)
                    showBlock("#success");
                else
                    showBlock("#fail");
            },
            error: function () {
                showBlock("#fail");
            }
        });

        event.preventDefault();
    });


});

function showBlock(block){
    $( block ).show( "clip", {}, 1000, callback );

    function callback() {
        setTimeout(function() {
            $( block + ":visible" ).removeAttr( "style" ).fadeOut();
        }, 2000 );
    }

}
