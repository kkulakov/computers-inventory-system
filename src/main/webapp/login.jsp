<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>Авторизація</title>
    <style>
        #auth_login_form {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -150px;
            margin-top: -90px;
            width: 300px;
            height: 180px;

            text-align: center;
            font-family: Tahoma, Verdana, Arial, sans-serif;
            font-size: 12px;

            border: 1px solid;
            /* experiments only for Firefox and Safari/Webkit */
            border-radius: 7px;
            -webkit-box-shadow: #888 3px 3px 3px;
        }
    </style>
</head>
<body>
<div align="center">
    <br>
    <br>
    <br>
    <h1>Система обліку комп’ютерної техніки</h1>
    <br>
    <small> Пароль: pass. Тільки нікому не кажіть :) </small>
</div>
<div id='auth_login_form'>
    <h3>Авторизація</h3>

    <c:if test="${not empty param.error}">
        <div style="color: red;"> Помилка авторизації
            : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </div>
    </c:if>
    <form method="POST" action="<c:url value="/j_spring_security_check" />">
        <table>
            <tr>
                <td align="right">Логін:</td>
                <td><input type="text" value="admin" disabled/><input type="text" name="j_username" value="admin" style="display: none;"/></td>
            </tr>
            <tr>
                <td align="right">Пароль: </td>
                <td><input type="password" name="j_password" /></td>
            </tr>
            <tr>
                <td align="right">Запам'ятати мене: </td>
                <td><input type="checkbox" name="_spring_security_remember_me" /></td>
            </tr>
            <tr>
                <td colspan="2" align="right"><input type="submit" value="Login" />
                    <input type="reset" value="Reset" /></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>